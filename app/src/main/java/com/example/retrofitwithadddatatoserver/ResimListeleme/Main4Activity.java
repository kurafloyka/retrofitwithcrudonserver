package com.example.retrofitwithadddatatoserver.ResimListeleme;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.retrofitwithadddatatoserver.Models.Urun;
import com.example.retrofitwithadddatatoserver.R;
import com.example.retrofitwithadddatatoserver.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main4Activity extends AppCompatActivity {

    ListView listView;
    List<Urun> urunList;
    UrunAdapter urunAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        define();
        request();
    }

    public void define() {

        listView = findViewById(R.id.list_view);
    }

    public void request() {

        urunList = new ArrayList<>();
        Call<List<Urun>> getProductList = ManagerAll.getInstance().getProductCall();

        getProductList.enqueue(new Callback<List<Urun>>() {
            @Override
            public void onResponse(Call<List<Urun>> call, Response<List<Urun>> response) {
                //Log.i("Response :", response.body().toString());

                if (response.isSuccessful()) {
                    urunList = response.body();
                    urunAdapter = new UrunAdapter(getApplicationContext(), urunList);
                    listView.setAdapter(urunAdapter);
                }

            }

            @Override
            public void onFailure(Call<List<Urun>> call, Throwable t) {

            }
        });

    }
}
