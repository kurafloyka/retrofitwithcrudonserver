package com.example.retrofitwithadddatatoserver.ResimListeleme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.retrofitwithadddatatoserver.Models.Urun;
import com.example.retrofitwithadddatatoserver.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UrunAdapter extends BaseAdapter {

    Context context;
    List<Urun> list;

    public UrunAdapter(Context context, List<Urun> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(context).inflate(R.layout.layout2, parent, false);

        ImageView urunResmi = convertView.findViewById(R.id.urunResmi);
        TextView id = convertView.findViewById(R.id.id);
        TextView urunAdi = convertView.findViewById(R.id.urunAdi);
        TextView urunFiyati = convertView.findViewById(R.id.urunFiyati);

        id.setText("Id : " + list.get(position).getId());
        urunAdi.setText("Urun Adi :" + list.get(position).getUrunadi());
        urunFiyati.setText("Urun Fiyati : " + list.get(position).getUrunfiyati());

        Picasso.with(context).load("http://kurafloyka.com/mobileToDB/resimler/" + list.get(position).getUrunresmi()).into(urunResmi);


        return convertView;
    }
}
