package com.example.retrofitwithadddatatoserver.ListelemeVeSilme;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.retrofitwithadddatatoserver.Models.User;
import com.example.retrofitwithadddatatoserver.R;

import java.util.List;

public class UserAdapter extends BaseAdapter {

    public UserAdapter(Context context, List<User> list, Activity activity) {
        this.context = context;
        this.list = list;
        this.activity = activity;
    }

    Context context;
    List<User> list;
    Activity activity;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);

        TextView id = convertView.findViewById(R.id.id);
        final TextView ad = convertView.findViewById(R.id.ad);
        TextView soyad = convertView.findViewById(R.id.soyad);

        id.setText(list.get(position).getId());
        soyad.setText(list.get(position).getSoyad());
        ad.setText(list.get(position).getAd());


        return convertView;
    }
}
