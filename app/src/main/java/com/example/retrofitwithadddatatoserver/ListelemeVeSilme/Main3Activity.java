package com.example.retrofitwithadddatatoserver.ListelemeVeSilme;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.retrofitwithadddatatoserver.Models.Result;
import com.example.retrofitwithadddatatoserver.Models.User;
import com.example.retrofitwithadddatatoserver.R;
import com.example.retrofitwithadddatatoserver.RestApi.ManagerAll;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main3Activity extends AppCompatActivity {
    ListView listView;
    List<User> userList;
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        define();
        request();
    }

    public void define() {

        listView = findViewById(R.id.userListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                show(userList.get(position).getId());
            }
        });
    }

    private void request() {


        Call<List<User>> getUsers = ManagerAll.getInstance().getUserListCall();

        getUsers.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {

                    userList = response.body();

                    userAdapter = new UserAdapter(getApplicationContext(), userList, Main3Activity.this);
                    listView.setAdapter(userAdapter);
                    //Log.i("response : ", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.i("response : ", t.toString());
            }
        });
    }



    public void show(final String id) {
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialoglayout, null);

        Button iptal = view.findViewById(R.id.iptalButton);
        Button sil = view.findViewById(R.id.silButton);
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog alertDialog = alert.create();

        iptal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        sil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<Result> deleteUserList = ManagerAll.getInstance().deleteUserCall(id);

                deleteUserList.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.isSuccessful()) {
                            request();
                            //Toast.makeText(DialogClass.this.getClass(),response.message(),Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        //Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                        Log.i("response : ", t.toString());
                    }
                });

                alertDialog.cancel();

            }
        });

        alertDialog.show();
    }
}
