package com.example.retrofitwithadddatatoserver.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urun {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("urunadi")
    @Expose
    private String urunadi;
    @SerializedName("urunfiyati")
    @Expose
    private String urunfiyati;
    @SerializedName("urunresmi")
    @Expose
    private String urunresmi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrunadi() {
        return urunadi;
    }

    public void setUrunadi(String urunadi) {
        this.urunadi = urunadi;
    }

    public String getUrunfiyati() {
        return urunfiyati;
    }

    public void setUrunfiyati(String urunfiyati) {
        this.urunfiyati = urunfiyati;
    }

    public String getUrunresmi() {
        return urunresmi;
    }

    public void setUrunresmi(String urunresmi) {
        this.urunresmi = urunresmi;
    }

    @Override
    public String toString() {
        return "Urun{" +
                "id='" + id + '\'' +
                ", urunadi='" + urunadi + '\'' +
                ", urunfiyati='" + urunfiyati + '\'' +
                ", urunresmi='" + urunresmi + '\'' +
                '}';
    }
}