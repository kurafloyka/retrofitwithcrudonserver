package com.example.retrofitwithadddatatoserver.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UyeBilgiler {

    @SerializedName("uyeismi")
    @Expose
    private String uyeismi;
    @SerializedName("uyeyasi")
    @Expose
    private String uyeyasi;
    @SerializedName("uyeokul")
    @Expose
    private String uyeokul;
    @SerializedName("uyeresim")
    @Expose
    private String uyeresim;
    @SerializedName("uyemailadres")
    @Expose
    private String uyemailadres;

    public String getUyeismi() {
        return uyeismi;
    }

    public void setUyeismi(String uyeismi) {
        this.uyeismi = uyeismi;
    }

    public String getUyeyasi() {
        return uyeyasi;
    }

    public void setUyeyasi(String uyeyasi) {
        this.uyeyasi = uyeyasi;
    }

    public String getUyeokul() {
        return uyeokul;
    }

    public void setUyeokul(String uyeokul) {
        this.uyeokul = uyeokul;
    }

    public String getUyeresim() {
        return uyeresim;
    }

    public void setUyeresim(String uyeresim) {
        this.uyeresim = uyeresim;
    }

    public String getUyemailadres() {
        return uyemailadres;
    }

    public void setUyemailadres(String uyemailadres) {
        this.uyemailadres = uyemailadres;
    }

    @Override
    public String toString() {
        return "UyeBilgiler{" +
                "uyeismi='" + uyeismi + '\'' +
                ", uyeyasi='" + uyeyasi + '\'' +
                ", uyeokul='" + uyeokul + '\'' +
                ", uyeresim='" + uyeresim + '\'' +
                ", uyemailadres='" + uyemailadres + '\'' +
                '}';
    }
}
