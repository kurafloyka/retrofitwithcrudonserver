package com.example.retrofitwithadddatatoserver.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sonuc {

    @SerializedName("Result")
    @Expose
    private Boolean result;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "Sonuc{" +
                "result=" + result +
                '}';
    }
}
