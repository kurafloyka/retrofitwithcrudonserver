package com.example.retrofitwithadddatatoserver.AddUser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.retrofitwithadddatatoserver.Models.Result;
import com.example.retrofitwithadddatatoserver.R;
import com.example.retrofitwithadddatatoserver.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {
    EditText ad, soyad;
    Button ekleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        define();
        addUser();

    }

    public void define() {

        ad = findViewById(R.id.ad);
        soyad = findViewById(R.id.soyad);
        ekleButton = findViewById(R.id.ekleButton);


    }

    public void addUser() {
        ekleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String adText = ad.getText().toString();
                String soyadText = soyad.getText().toString();

                if (!adText.equals("") && !soyadText.equals("")) {
                    request(adText, soyadText);
                    ad.setText("");
                    soyad.setText("");
                } else {
                    Toast.makeText(getApplicationContext(), "Zorunlu alanlari giriniz...", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    public void request(String ad, String soyad) {

        Call<Result> addUser = ManagerAll.getInstance().addUserResultConCall(ad, soyad);
        addUser.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.isSuccessful()) {

                    Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.i("response : ", t.toString());
            }
        });

    }
}
