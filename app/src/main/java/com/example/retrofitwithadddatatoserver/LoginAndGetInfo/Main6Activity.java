package com.example.retrofitwithadddatatoserver.LoginAndGetInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.retrofitwithadddatatoserver.Models.Uye;
import com.example.retrofitwithadddatatoserver.R;
import com.example.retrofitwithadddatatoserver.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main6Activity extends AppCompatActivity {

    EditText username, password;
    Button login;
    String usernameText, passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        define();
        action();
    }


    public void define() {
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
    }


    public void action() {


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                request();
            }
        });
    }


    public void request() {
        usernameText = username.getText().toString();
        passwordText = password.getText().toString();
        Call<Uye> uyeCall = ManagerAll.getInstance().sendUserInfoCall(usernameText, passwordText);

        uyeCall.enqueue(new Callback<Uye>() {
            @Override
            public void onResponse(Call<Uye> call, Response<Uye> response) {

                if (response.isSuccessful()) {

                    Log.i("response", response.body().getId());

                    String id = response.body().getId();
                    Intent intent = new Intent(Main6Activity.this, Main7Activity.class);
                    intent.putExtra("id", id);
                    startActivity(intent);

                }
            }

            @Override
            public void onFailure(Call<Uye> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                Log.i("response : ", t.toString());
            }
        });

    }
}
