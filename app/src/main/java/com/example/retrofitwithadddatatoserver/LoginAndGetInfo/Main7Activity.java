package com.example.retrofitwithadddatatoserver.LoginAndGetInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.retrofitwithadddatatoserver.Models.UyeBilgiler;
import com.example.retrofitwithadddatatoserver.R;
import com.example.retrofitwithadddatatoserver.RestApi.BaseUrl;
import com.example.retrofitwithadddatatoserver.RestApi.ManagerAll;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main7Activity extends AppCompatActivity {

    String id;

    TextView uyeIsmi, uyeYasi, uyeOkul, uyeResim, uyeEmailAdres;
    ImageView uyeResimImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);

        define();
        getId();
        request();


    }

    public void define() {
        uyeIsmi = findViewById(R.id.uyeIsmi);
        uyeYasi = findViewById(R.id.uyeYasi);
        uyeOkul = findViewById(R.id.uyeOkul);
        uyeResim = findViewById(R.id.uyeResim);
        uyeEmailAdres = findViewById(R.id.uyeMailAdresi);
        uyeResimImage = findViewById(R.id.uyeResimImage);
    }


    public void getId() {
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        Log.i("response", id);
    }

    public void request() {


        Call<UyeBilgiler> uyeBilgilerCall = ManagerAll.getInstance().getUserInfoCall(id);
        uyeBilgilerCall.enqueue(new Callback<UyeBilgiler>() {
            @Override
            public void onResponse(Call<UyeBilgiler> call, Response<UyeBilgiler> response) {
                if (response.isSuccessful()) {

                    Log.i("response", response.body().toString());

                    uyeIsmi.setText("Uye ismi : " + response.body().getUyeismi());
                    uyeYasi.setText("Uye yasi : " + response.body().getUyeyasi());
                    uyeOkul.setText("Uye okul : " + response.body().getUyeokul());
                    uyeResim.setText("Uye resim : " + response.body().getUyeresim());
                    uyeEmailAdres.setText("Uye email adresi :" + response.body().getUyemailadres());


                    //Log.i("response",BaseUrl.postUrl + "resimler/" + response.body().getUyeresim());
                    Picasso.with(getApplicationContext()).load(BaseUrl.postUrl + "resimler/" + response.body().getUyeresim()).into(uyeResimImage);
                }
            }

            @Override
            public void onFailure(Call<UyeBilgiler> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString() + " : Ilgili kullaniciya ait bilgi bulunamadi...", Toast.LENGTH_LONG).show();
                //Log.i("response : ", t.toString());
            }
        });

    }
}
