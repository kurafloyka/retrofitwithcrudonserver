package com.example.retrofitwithadddatatoserver.RestApi;


import com.example.retrofitwithadddatatoserver.Models.Result;
import com.example.retrofitwithadddatatoserver.Models.Sonuc;
import com.example.retrofitwithadddatatoserver.Models.Urun;
import com.example.retrofitwithadddatatoserver.Models.User;
import com.example.retrofitwithadddatatoserver.Models.Uye;
import com.example.retrofitwithadddatatoserver.Models.UyeBilgiler;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApi {
    @FormUrlEncoded
    @POST("ekle.php")
    Call<Result> addUser(@Field("ad") String ad, @Field("soyad") String soyad);

    @FormUrlEncoded
    @POST("sartliekle.php")
    Call<Result> addUserCondition(@Field("ad") String ad, @Field("soyad") String soyad);

    @GET("listele.php")
    Call<List<User>> getUserList();

    @FormUrlEncoded
    @POST("sil.php")
    Call<Result> deleteUser(@Field("id") String id);


    @GET("urunler.php")
    Call<List<Urun>> getProductList();


    @FormUrlEncoded
    @POST("resimekle.php")
    Call<Result> addImage(@Field("baslik") String baslik, @Field("image") String image);

    @FormUrlEncoded
    @POST("girisyap.php")
    Call<Uye> sendUserInfo(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("bilgigetir.php")
    Call<UyeBilgiler> getUserInfo(@Field("uyeid") String uyeId);

    @GET("/bilgigetir.php")
    Call<List<UyeBilgiler>> getUserInfoGet(@Query("uyeid") String id);


    @FormUrlEncoded
    @POST("mailgonder.php")
    Call<Sonuc> addUserRegister(@Field("kullaniciadi") String kullaniciadi,@Field("sifre") String sifre,@Field("mailadres") String mailadres);

}




