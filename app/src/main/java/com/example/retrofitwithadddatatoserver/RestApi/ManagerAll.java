package com.example.retrofitwithadddatatoserver.RestApi;


import com.example.retrofitwithadddatatoserver.Models.Result;
import com.example.retrofitwithadddatatoserver.Models.Sonuc;
import com.example.retrofitwithadddatatoserver.Models.Urun;
import com.example.retrofitwithadddatatoserver.Models.User;
import com.example.retrofitwithadddatatoserver.Models.Uye;
import com.example.retrofitwithadddatatoserver.Models.UyeBilgiler;

import java.util.List;

import retrofit2.Call;

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }


    public Call<Result> addUserResultCall(String ad, String soyad) {
        Call<Result> addUserResult = getRestApiClient().addUser(ad, soyad);
        return addUserResult;
    }


    public Call<Result> addUserResultConCall(String ad, String soyad) {
        Call<Result> addUserResult = getRestApiClient().addUserCondition(ad, soyad);
        return addUserResult;
    }


    public Call<List<User>> getUserListCall() {
        Call<List<User>> getUserListResult = getRestApiClient().getUserList();
        return getUserListResult;
    }

    public Call<Result> deleteUserCall(String id) {

        Call<Result> deleteUserList = getRestApiClient().deleteUser(id);
        return deleteUserList;
    }


    public Call<List<Urun>> getProductCall() {

        Call<List<Urun>> getProductList = getRestApiClient().getProductList();
        return getProductList;
    }

    public Call<Result> addImageCall(String baslik, String image) {

        Call<Result> addImageResult = getRestApiClient().addImage(baslik, image);
        return addImageResult;
    }


    public Call<Uye> sendUserInfoCall(String username, String password) {

        Call<Uye> senUserInfoResult = getRestApiClient().sendUserInfo(username, password);
        return senUserInfoResult;
    }

    public Call<UyeBilgiler> getUserInfoCall(String uyeId) {

        Call<UyeBilgiler> getUserInfoResult = getRestApiClient().getUserInfo(uyeId);
        return getUserInfoResult;
    }

    public Call<Sonuc> addUserRegisterCall(String kullaniciadi, String sifre, String mailadres) {

        Call<Sonuc> addUserRegisterResult = getRestApiClient().addUserRegister(kullaniciadi, sifre, mailadres);
        return addUserRegisterResult;
    }
}
