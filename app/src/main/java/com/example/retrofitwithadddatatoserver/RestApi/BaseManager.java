package com.example.retrofitwithadddatatoserver.RestApi;

public class BaseManager {


    protected RestApi getRestApiClient() {

        RestApiClient restApiClient = new RestApiClient(BaseUrl.postUrl);

        return restApiClient.getRestApi();
    }
}
