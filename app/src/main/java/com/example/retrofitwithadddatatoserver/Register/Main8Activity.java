package com.example.retrofitwithadddatatoserver.Register;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.retrofitwithadddatatoserver.Models.Sonuc;
import com.example.retrofitwithadddatatoserver.R;
import com.example.retrofitwithadddatatoserver.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main8Activity extends AppCompatActivity {

    EditText kullaniciAdi, sifre, mailAdres;
    Button register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main8);

        define();
        action();
    }


    public void define() {

        kullaniciAdi = findViewById(R.id.kullaniciadi);
        sifre = findViewById(R.id.sifre);
        mailAdres = findViewById(R.id.mailadres);
        register = findViewById(R.id.register);
    }

    public void action() {

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String kullaniciAdiText = kullaniciAdi.getText().toString();
                String sifreText = sifre.getText().toString();
                String mailAdresText = mailAdres.getText().toString();

                if(!kullaniciAdiText.equals("")&&!sifreText.equals("")&&!mailAdresText.equals("")){
                Call<Sonuc> registerCall = ManagerAll.getInstance().addUserRegisterCall(kullaniciAdiText, sifreText, mailAdresText);

                registerCall.enqueue(new Callback<Sonuc>() {
                    @Override
                    public void onResponse(Call<Sonuc> call, Response<Sonuc> response) {
                        if (response.isSuccessful()) {


                            Toast.makeText(getApplicationContext(), "Aktivasyon maili gonderildi", Toast.LENGTH_LONG).show();
                            //Log.i("response : ", response.body().getResult().toString());

                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Mail alani zaten kayitli", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Sonuc> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
                        Log.i("response : ", t.toString());
                    }
                });}else{
                    Toast.makeText(getApplicationContext(), "Bilgileri Dogru Giriniz....", Toast.LENGTH_LONG).show();

                }


            }
        });
    }
}
